<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="Style/style.css">
    <meta charset="UTF-8">
     <title>Klienci</title>
</head>
<style>
    
</style>
<body>
    
   <?php
    include("Include/menu.php");
   ?>
    
   
<?php

$result = GetClientData($connect);


?> 
<div>
<table align = "center">
    <tr>
        <th>
            Klient
        </th> 
        <th>
            Pakiet
        </th> 
        <th>
            Rozpoczęcie współpracy 
        </th> 
        <th>
            Zakończenie współpracy 
        </th> 
        <th>
            Osoby kontaktowe 
        </th> 
        <th>
            Opiekunowie z agencji 
        </th> 
    </tr>
<?php
    while($row = $result -> fetch_assoc() )
    {

        echo " 
            <tr>
                <td>$row[company_name] </td>
                <td>$row[package_type] </td>
                <td>$row[starting_date] </td>
                <td>$row[expiration_date] </td>
                <td>
                " ;
            $contactResult = fetchContactData("$row[id]",$connect );
            while($rowConctact = $contactResult -> fetch_assoc()){
                echo "$rowConctact[name] $rowConctact[surname] email: $rowConctact[mail] telefon: $rowConctact[phone] <br />";
            }
            
        echo    "</td>
                <td>"; 
            $representativeResult = fetchAgencyRepresentativeData("$row[id]",$connect );
            while($rowRepresentative = $representativeResult -> fetch_assoc()){
                echo "$rowRepresentative[name] $rowRepresentative[surname] $rowRepresentative[mail] $rowRepresentative[phone]<br />";
            }    
        echo    "</td>
            </tr>
        
        ";
    }
?>
</table>
<br>
</div>
</body>
</html>