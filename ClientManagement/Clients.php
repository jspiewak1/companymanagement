<?php
   require_once("Controllers/dbController.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="Style/style.css">
    <meta charset="UTF-8">
     <title>Klienci</title>
</head>
<body align ="center">
<?php
include("Include/menu.php");
$sleepTime = 5;
if(!isset($_GET['agentID'])){
    echo"No ID was sent";
    sleep($sleepTime);
    header("location: index.php");
}
$id = $_GET['agentID'];
$agentResult = fetchOneRepresentative($id, $connect);

?>
<table border = "1px" align = "center">
    <tr>
        <th>
            Nazwa Firmy
        </th>
        <th>
            Typ Pakietu
        </th>
        <th>
            Data rozpoczęcia 
        </th>
        <th>
            Data Wygaśnięcia
        </th>
    </tr>
<?php
    while($agentRow = $agentResult->fetch_assoc()){
        echo "
        <tr>
            <td>
                $agentRow[company_name]
            </td>
            <td>
                $agentRow[package_type]
            </td>
            <td>
                $agentRow[starting_date]
            </td>
            <td>
                $agentRow[expiration_date]
            </td>
        </tr>
        ";
    }
?>

</table>

</body> 
</html>