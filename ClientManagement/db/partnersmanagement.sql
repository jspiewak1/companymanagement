-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Paź 03, 2023 at 06:45 AM
-- Wersja serwera: 10.4.28-MariaDB
-- Wersja PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `partnersmanagement`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `company_name` text NOT NULL,
  `package_type` int(11) DEFAULT NULL,
  `starting_date` date DEFAULT NULL,
  `expiration_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `company_name`, `package_type`, `starting_date`, `expiration_date`) VALUES
(1, 'CompanyA', 1, '2023-09-04', '2023-09-30'),
(2, 'CompanyB', 2, '2023-09-03', '2025-09-01'),
(3, 'CompanyC', 1, '2023-08-01', '2023-12-29'),
(20, 'CompanyD', 1, '2023-10-02', '2023-10-31'),
(21, 'CompanyF', 2, '2023-10-02', '2023-11-03'),
(22, 'CompanyF', 2, '2023-10-02', '2023-10-31');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `client_contacts`
--

CREATE TABLE `client_contacts` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `surname` text DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `mail` text DEFAULT NULL,
  `phone` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `client_contacts`
--

INSERT INTO `client_contacts` (`id`, `name`, `surname`, `company_id`, `mail`, `phone`) VALUES
(1, 'Jan ', 'Nowakowski ', 1, 'Jan.Now@mail.com', 111222333),
(2, 'Tomasz', 'Kowalski ', 2, 'Tomasz.kow@mail.com', 222333444),
(3, 'Kamil', 'Smith', 3, 'Kamil.Smith@mail.com', 222444333),
(4, 'Robert', 'Zieliński', 1, 'rob.ziel@mail.com', 444555666),
(7, 'Jan', 'Śpiewak', 20, 'janspiewak99@gmail.com', 792533565),
(8, 'Borys', 'Krawczyk', 21, 'Bo.kra@mail.com', 123954293),
(9, 'Elżbieta ', 'Ciesielska', 21, 'El.cies@mail.com', 12935324),
(10, 'Joanna', 'Mukala', 22, 'Joa.Muk@mail.com', 1234567864);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `surname` text DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `mail` text DEFAULT NULL,
  `salary` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `surname`, `phone`, `mail`, `salary`) VALUES
(1, 'Jan', 'Nowak', 111222333, 'Jan@mail.com', 4000),
(2, 'Piotr', 'Kowal', 133422533, 'Piotr.Kowal@mail.com', 4444),
(3, 'Witold', 'Sławny', 444223423, 'wit.slaw@mail.com', 5500);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `employees_clients`
--

CREATE TABLE `employees_clients` (
  `employee_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `employees_clients`
--

INSERT INTO `employees_clients` (`employee_id`, `client_id`, `id`) VALUES
(1, 1, 1),
(2, 2, 2),
(1, 3, 3),
(2, 1, 4),
(3, 3, 5),
(1, 20, 18),
(2, 20, 19),
(1, 21, 20),
(2, 21, 21),
(1, 22, 22),
(2, 22, 23),
(3, 22, 24);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `package_types`
--

CREATE TABLE `package_types` (
  `id` int(11) NOT NULL,
  `type` text DEFAULT NULL,
  `pricing` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `package_types`
--

INSERT INTO `package_types` (`id`, `type`, `pricing`) VALUES
(1, 'TypeA', 1000),
(2, 'TypeB', 2000);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `packageType` (`package_type`);

--
-- Indeksy dla tabeli `client_contacts`
--
ALTER TABLE `client_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indeksy dla tabeli `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `employees_clients`
--
ALTER TABLE `employees_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indeksy dla tabeli `package_types`
--
ALTER TABLE `package_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `client_contacts`
--
ALTER TABLE `client_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employees_clients`
--
ALTER TABLE `employees_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `package_types`
--
ALTER TABLE `package_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`package_type`) REFERENCES `package_types` (`id`);

--
-- Constraints for table `client_contacts`
--
ALTER TABLE `client_contacts`
  ADD CONSTRAINT `client_contacts_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `clients` (`id`);

--
-- Constraints for table `employees_clients`
--
ALTER TABLE `employees_clients`
  ADD CONSTRAINT `employees_clients_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `employees_clients_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
