<?php

function isValidDate($date) {
    return (strtotime($date) !== false);
}
function isNumeric(... $numberCheck){
    $variableIsNumeric = true;
    foreach($numberCheck as $value){
        $number =(int)$value;
        if(!is_numeric($number)){
            $variableIsNumeric = false;
            break;
        }
    }
    
    return $variableIsNumeric;
}
function isMail(... $mailCheck){
    $variableIsMail = true;
    foreach($mailCheck as $value){
        if(filter_var($value, FILTER_VALIDATE_EMAIL)){
            $variableIsMail = false;
            break;
        }
    }
    return $variableIsMail;
}
function emptyCheck(... $emptyCheck){
    $variableIsEmpty = false;
    foreach($emptyCheck as $checkData){
        if(empty($checkData)){ //the variable has data 
            $variableIsEmpty = true;
            break;
        }   
    }
    return $variableIsEmpty;
}

?>

