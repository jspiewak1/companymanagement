<?php 
    declare(strict_types = 1);
    function ConnectToDB(){  //connection to database
        $connection = new mysqli('localhost','root' ,'' ,'partnersmanagement');
        if($connection->connect_error){
            die("no connection with server");
        }else {
            return $connection;
        }
    }
    function GetClientData(mysqli $connection){  //downloads data about client
        return SelectQuerries("Select * from clients", $connection);
        
    }
    function fetchContactData($id, mysqli $connection){  //fetch data about contacts to clients 
        $sqlQuery = "SELECT * FROM client_contacts where company_id = $id";
        return SelectQuerries($sqlQuery, $connection);
    }
    function fetchAgencyRepresentativeData($id, mysqli $connection){ //fetch data about representatives
        $sqlQuery = "SELECT e.name, e.surname, e.mail, e.phone FROM employees e join employees_clients ec on e.id = ec.employee_id where ec.client_id = $id";
        return SelectQuerries($sqlQuery, $connection);
    }
    function fetchAllRepresentative(mysqli $connection){
        $sqlQuery = "SELECT id, name, surname  FROM employees";
        return SelectQuerries($sqlQuery, $connection);
    }
    function fetchOneRepresentative($id, mysqli $connection){
        $sqlQuery = "SELECT c.company_name, c.package_type, c.starting_date, c.expiration_date FROM clients c join employees_clients ec on c.id = ec.client_id where ec.employee_id = $id";
        return SelectQuerries($sqlQuery, $connection);
    }
    function fetchAllContacts(mysqli $connection){
        $sqlQuery = "SELECT cc.name, cc.surname, cc.mail, cc.phone, c.company_name FROM client_contacts cc join clients c on cc.company_id=c.id ";
        return SelectQuerries($sqlQuery, $connection);
    }
    function fetchAllPackages(mysqli $connection){
        $sqlQuery = "SELECT * FROM package_types ";
        return SelectQuerries($sqlQuery, $connection);
    }
    function GetLastID(mysqli $connection){
        $sqlQuery = "SELECT id FROM clients ORDER BY id DESC LIMIT 1";
        $result = SelectQuerries($sqlQuery, $connection);
        $lastID = $result -> fetch_row();
        return $lastID[0];
    }
    function CheckAgentID(mysqli $connection, $id){
        $sqlQuery = "SELECT * FROM employees WHERE id = $id ";
        $result = SelectQuerries($sqlQuery, $connection);
        if(mysqli_num_rows($result) ==1){
            return true;
        }
        else{
            return false;
        }
    }
    function SelectQuerries(string $query, mysqli $connection){  //querry controller
        $sqlQuery = $query;
        $result = $connection->query($sqlQuery);
        if(!$result)
        {
            die("Invalid Query" . $connection -> error);
        }
        return $result;
    }
    function InsertQuerries(string $query, mysqli $connection){
        $result = $connection -> query($query);
        if(!$result){
            die("Invalid Query". $connection ->error);
        }
       
    }
    /*

    */

?>
