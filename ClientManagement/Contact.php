<?php
   require_once("Controllers/dbController.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="Style/style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kontakt</title>
</head>
<body>
<?php
    include("Include/menu.php");
    $contactResult = fetchAllContacts($connect);
?>
<table align= "center">
    <tr>
        <th>
            Imie
        </th>
        <th>
            Nazwisko
        </th>
        <th>
            Mail
        </th>
        <th>
            Telefon
        </th>
        <th>
            Nazwa firmy
        </th>
    </tr>
<?php
    while($contactRow = $contactResult -> fetch_assoc()){
        echo"
        <tr>
            <td> 
                $contactRow[name]
            </td>
            <td> 
                $contactRow[surname]
            </td>
            <td> 
                $contactRow[mail]
            </td>
            <td> 
                $contactRow[phone]
            </td>
            <td> 
                $contactRow[company_name]
            </td>
        </tr>
        ";
            
    }
 ?>
        
    </tr>
</table>
</body>
</html>