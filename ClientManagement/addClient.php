<?php
require_once("Controllers/dbController.php");
require_once("Controllers/validateController.php");
$errorMessage = '';

if($_SERVER['REQUEST_METHOD']== 'POST'){
    $connect = ConnectToDB();
    do{
        
        
        if(!isset($_POST["name"]) ||!isset($_POST["package"]) ||!isset($_POST["starting_date"]) ||!isset($_POST["expiration_date"]) || !isset($_POST["numberOfContacts"])){ //checks if name of input wasnt changed  
            $errorMessage = "Niepoprawna nazwa zmiennych";
            break;
        }

        $contacts = $_POST["numberOfContacts"];  //variable that stores amount of contacts send by user 
        if(!isNumeric($contacts) && $contacts<1){ //checks if contacts arent numbers
            $errorMessage = "Niepoprawna liczba kontaktów ";
            break;
        }
        $contactNames = []; //contact Arrays declaration 
        $contactSurnames = [];
        $contactMails = [];
        $contactPhones = [];
        $areContactsValid = true;
       //echo $contacts,   $_POST["contactName1"],   $_POST["contactName2"];
        for($i = 0 ;$i< $contacts; $i++){  //adds contacts to arrays
            $counter = $i+1;
            if(isset($_POST["contactName$counter"]) && isset($_POST["contactSurname$counter"]) && isset($_POST["contactMail$counter"]) &&isset($_POST["contactPhone$counter"])){
                array_push($contactNames, mysqli_real_escape_string($connect, $_POST["contactName$counter"]));
                array_push($contactSurnames, mysqli_real_escape_string($connect, $_POST["contactSurname$counter"]));
                array_push($contactMails, mysqli_real_escape_string($connect, $_POST["contactMail$counter"]));
                array_push($contactPhones, mysqli_real_escape_string($connect, $_POST["contactPhone$counter"]));
            } else{
                $areContactsValid = false;
                break;
            }
        }
        if(!$areContactsValid){ 
            $errorMessage = "Błąd wpisywania danych kontaktów";
            break;
        }
       
        if(!isNumeric($contactPhones)){  //they are on validateController. Its easier for checking arrays
            $errorMessage = "Numery telefonów nie są liczbami całkowitymi";
            break;
        }
        if(!isMail($contactMails)){ //also on validateController because of arrays
            $errorMessage = "Wysłane adresy E-mail nie są poprawne";
            break;
        }
        $representativeCount = mysqli_num_rows(fetchAllRepresentative($connect)); //checks total number of representatives, sending this via html could generate error if user changed data in browser
        $agents = [];
        for($i = 0;$i<$representativeCount;$i++){ 
            $agentNumber = $i+1;
            if(isset($_POST["agentID$agentNumber"])){
                array_push($agents, mysqli_real_escape_string($connect, $_POST["agentID$agentNumber"])); //pushes only agents that were checked
            }
        }

        if(count($agents)<1){ //checks if any agents were assigned in form 
            
            $errorMessage = "żaden opiekun nie został wysłany";
            break;
        }
       $idNotFound = false; 
        for($i = 0;$i<count($agents);$i++){ 
            if(!CheckAgentID($connect, $agents[$i])){ //checks if agentID is in database
                $idNotFound = true;
            }
        }
        if($idNotFound){    
            $errorMessage = "niepoprawne ID opiekuna";
            break;
        }

        $name = mysqli_real_escape_string($connect, $_POST["name"]);    
        $package = mysqli_real_escape_string($connect, $_POST["package"]);
        $startingDate =mysqli_real_escape_string($connect, $_POST["starting_date"]);
        $expirationDate =mysqli_real_escape_string($connect, $_POST["expiration_date"]);
 
        if(emptyCheck($name, $package, $startingDate, $expirationDate)){ //checks if any data was left empty
            $errorMessage = 'Wszystkie pola muszą zostać wypełnione';
            break;
        }
        if (!isValidDate($startingDate) || !isValidDate($expirationDate)) { //checks if date is date 
            $errorMessage = 'Niepoprawna data';
            break;
        }
        if($startingDate >$expirationDate){ //checks if starting date is not higher than expirationdate
            $errorMessage = 'Data rozpoczęcia nie może być po dacie zakończenia współpracy';
            break;
        }
        $query = "SELECT * from package_types where id = '$package'"; //checks if user didnt change data in browser 
        $packageExists = SelectQuerries($query, $connect);
        if($packageExists->num_rows==0){
            $errorMessage = "Taki typ nie istnieje";
            break;
        }
        $query = "INSERT INTO clients (company_name, package_type, starting_date, expiration_date) VALUES('$name', $package, '$startingDate', '$expirationDate')"; //query that adds new client
       // echo $query; 
        InsertQuerries($query, $connect);
        $lastID = GetLastID($connect);
        
        foreach($agents as $value){ //assigns agents to clients (wasnt sure if it should be programmed so i prefered to add this)
            $query = "INSERT INTO employees_clients (employee_id, client_id) VALUES($value, $lastID)";
            InsertQuerries($query, $connect);
        }
        for($i = 0;$i< $contacts;$i++){
            
            $query = "INSERT INTO client_contacts(name, surname,company_id, mail, phone) VALUES('$contactNames[$i]','$contactSurnames[$i]',$lastID,'$contactMails[$i]','$contactPhones[$i]')";
            InsertQuerries($query, $connect);
        }
        header("location: index.php");
    }
    while(false);
}
//echo $name, $package, $startingDate, $expirationDate;


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="Style/style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
    include("Include/menu.php");
    echo $errorMessage;    
?>
<form method="post">
    <div class ="container">
        <div class ="row">
            <div class ="col-md3">
                <h4> 
                    Informacje o firmie 
                </h4>
            Nazwa Firmy: <input type="text" name = "name"/><br />
           Typ Pakietu: 
                <select name="package" id="package">
            <?php
                $packagesResult = fetchAllPackages($connect);
                while($rowPackages = $packagesResult ->fetch_assoc()){
                    echo"<option value ='$rowPackages[id]'>$rowPackages[type] </option>";
                }
            ?>
                </select><br />
                 Data Rozpoczęcia: <input type="date" name = "starting_date"/><br />
                 Data Zakończcenia:<input type="date" name = "expiration_date"/><br />
            </div>
            <div class="col-md3">
                <h4>
                    Opiekunowie z agencji: 
                </h4>
            <?php
                $agentsCount =0;
                $agents = fetchAllRepresentative($connect);
                while($rowAgents = $agents ->fetch_assoc()){
                    $agentsCount++;
                    echo"
                            $rowAgents[name] $rowAgents[surname] 
                            <input type='checkbox' name='agentID$agentsCount' value='$rowAgents[id]' /> <br />
                    ";
                }
               
            ?>
            </div>
            <div class="col-md3">
                <h4>
                    Osoby Kontaktowe
                </h4>
                <label for="repeatCount">Enter a number:</label>
                <input type="number" id="repeatCount" name ="numberOfContacts" min="1" value="0">
                <input type="hidden" id="iterationCount" value="1">
                <div id="output"></div>
            
                <script>
                    const inputElement = document.getElementById('repeatCount');
                    const iterationInput = document.getElementById('iterationCount');
                    const outputDiv = document.getElementById('output');
                    
                    inputElement.addEventListener('input', repeatCode);
            
                    function repeatCode() {
                        const repeatCount = parseInt(inputElement.value);
                        iterationInput.value = repeatCount; // Set the hidden input value
                        outputDiv.innerHTML = '';
            
                        for (let i = 0; i < repeatCount; i++) {
                            const lineOfHtml = `
                                Osoba Kontaktowa ${i+1} <br />
                                Imie <input type="text" name = "contactName${i +1}"> <br />
                                Nazwisko <input type="text" name = "contactSurname${i +1}"> <br /> 
                                E-mail <input type = "email" name = "contactMail${i +1}" /> <br />
                                Telefon <input type = "number" name = "contactPhone${i +1}" /> <br />
                                
                            `;
                            outputDiv.innerHTML += lineOfHtml;
                        }
                    }
                </script>
            <button type="submit">Dodaj</button>
            </div>
                <br><br>
    </div>
</div>
    
   
</form>
</body>
</html>